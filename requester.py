import logging

import requests as rq


class Requester:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.logger.info("Init requester")

    def get(self, url, data):
        # if data is not None:
        response = rq.get(url) if data is None else rq.get(url, params=data)

        self.logger.info(response.url)
        return response

    # def post(self, url, data):
    #     response = rq.post(url, data=data)
    #     return response

    def post(self, url, data, file):
        response = rq.post(url, data=data) if file is None else rq.post(url, data=data, files = file)
        return response
