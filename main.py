from biz_controller import BizController
import logging
import logging.config
import os


def conf_log():
    log_filename = "logs/seeding.log"
    os.makedirs(os.path.dirname(log_filename), exist_ok=True)
    logging.config.fileConfig('logging.conf')
    logging.root.setLevel(logging.NOTSET)


if __name__ == '__main__':
    conf_log()

    biz = BizController()
    biz.comment_by_list()


