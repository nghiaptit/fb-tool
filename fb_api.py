import logging

import jsons
import common.url
from requester import Requester


class FacebookApi:

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.request = Requester()

    def get_comment(self, access_token):
        comment_id = '103035501704269_106327641375055'

        url = common.url.GET_COMMENT
        url = url.replace('{comment-id}', comment_id)
        data = {
            'access_token': access_token
        }
        res = self.request.get(url, data)

        self.logger.info(res.text)

    def post_comment(self, comment_id, data, image, access_token):
        url = common.url.POST_COMMENT
        url = url.replace('{object-id}', comment_id)

        params = jsons.load(data)
        params['access_token'] = str(access_token).strip()

        res = self.request.post(url, params, image)
        return res
