import logging
import time
from configparser import ConfigParser

import numpy
import pandas as pd
import random

from fb_api import FacebookApi
from excel.exceler import ExcelReader


class BizController:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.logger.warning("----------------Init Reply Bot----------------------")
        self.logger.info("Init controller")
        self.fb = FacebookApi()

    def comment_by_list(self, ):
        config = ConfigParser()
        config.read('config.ini')

        excel_path = config.get('main', 'path_excel')
        excel_reader = ExcelReader()

        input_data = excel_reader.read_file(excel_path)

        input_run = input_data[input_data['post_id'].notnull()]

        for index in input_run.index:
            comment = input_run.loc[index]['comment']
            url_image = input_run.loc[index]['url_image']
            post_ids = input_run.loc[index]['post_id']

            access_token_key = input_run.loc[index]['access_token']
            access_token = config.get('main', str(access_token_key).strip())

            list_post_id = str(post_ids).split('|')

            for post_id in list_post_id:
                param = {
                    'message': comment
                }

                image_file = None
                if str(url_image).lower() != 'nan':
                    try:
                        image_file = {'file': open(url_image, 'rb')}
                    except Exception as e:
                        self.logger.exception(e)

                res = self.fb.post_comment(post_id, param, image_file, access_token)
                self.logger.info(res.text)

                time_delay = random.randint(5, 8)
                time.sleep(time_delay)

                # print(param)
