import logging

import pandas as pd


class ExcelReader:
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def read_file(self, path):
        try:
            data = pd.read_excel(path, engine='openpyxl')
            return data
        except Exception as e:
            self.logger.error("Error when read file: " + path)
        return None
